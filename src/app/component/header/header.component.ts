import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../service/dataService/dataService';
import { ConfirmationDialogService } from 'src/app/component/confirmation-dialog/confirmation-dialog.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public sessionUser: any = {};

  constructor(
    public router: Router,
    public dataService:DataService,
    public confirmationDialogService: ConfirmationDialogService,
  ) {
    
    this.sessionUser = JSON.parse(localStorage.getItem('session'));

  /***
   * When user is logged that time we update session
   * 
   */
    this.dataService.getDataMessage().subscribe((msg) => {
      if(msg == 'logged'){
            this.sessionUser = JSON.parse(localStorage.getItem('session'));
      }
     });
   }

  ngOnInit(): void {}

   /***
   * This method use to logout user from current session
   * 
   */
  logOut(){
    this.confirmationDialogService.yesNoConfirm('Are you sure you want to Logout?..','')
    .then((confirmed) => {
      if (confirmed == true) {
        localStorage.clear();
        this.router.navigate(["/app/login"]);
      }
    })
  }

}

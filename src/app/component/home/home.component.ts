import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../service/dataService/dataService';
import { ConfirmationDialogService } from 'src/app/component/confirmation-dialog/confirmation-dialog.service';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public formDATA:any = {address:{}};
  public empList:any = [];
  public apiResponse:any = {};
  public sessionUser: any = {};


  constructor(
    public router: Router,
    public confirmationDialogService: ConfirmationDialogService,
    public dataService:DataService
  ) {
    this.sessionUser = JSON.parse(localStorage.getItem('session'));
    this.formDATA.managerId = this.sessionUser.userId;
  }
  ngOnInit() {
    this.dataService.appConstant.emailPattern;
    this.getEmpList();
  }


 /***
   * This method use to get all employee record manager wise
   * 
   */
  getEmpList() {
    this.dataService.appConstant.showLoader();
    this.dataService.getDATA(this.dataService.appConstant.API["LIST_OF_EMPLOYEE"]+this.sessionUser.userId).subscribe(response => {
      this.apiResponse = response;
      this.dataService.appConstant.hideLoader();
      if (this.apiResponse.status === 200) {
        this.empList = this.apiResponse.response;
      }
    })
  }


/**
 * This Method is call when we submit employee form
 * This method 1st check its empId present in object or not
 * If empId present in object then it will update else it call add new
 */

  submit():void{   
    if(this.formDATA.empId){
      this.confirmationDialogService.yesNoConfirm('Are you sure you want to update record?..','')
      .then((confirmed) => {
        if (confirmed == true) {
          this.updateEmployee();
        }
      })
    }else{
      this.addEmployee();
    }
  }


  /***
   * This method use to add new record
   * 
   */
  addEmployee() {
    this.dataService.appConstant.showLoader();
    this.dataService.postDATA(this.dataService.appConstant.API["ADD_EMPLOYEE"],this.formDATA).subscribe(response => {
      this.apiResponse = response;
      this.dataService.appConstant.hideLoader();
      if (this.apiResponse.status === 200) {
        this.dataService.appConstant.successTost("Employee Added Successfully")
        $("#myModal").modal("hide");
        this.getEmpList();
      }
    })
  }

   /***
   * This method use to Update record
   * 
   */
  updateEmployee() {
    this.dataService.appConstant.showLoader();
    this.dataService.putDATA(this.dataService.appConstant.API["UPDATE_EMPLOYEE"],this.formDATA).subscribe(response => {
      this.apiResponse = response;
      this.dataService.appConstant.hideLoader();
      if (this.apiResponse.status === 200) {
        this.dataService.appConstant.successTost("Employee Updated Successfully")
        $("#myModal").modal("hide");
        this.getEmpList();
      }
    })
  }

   /***
   * This method use to Delete record
   * 
   */
  deleteEmp(id) {
    this.confirmationDialogService.yesNoConfirm('Are you sure you want to delete record?..','')
    .then((confirmed) => {
      if (confirmed == true) {
        this.dataService.appConstant.showLoader();
        this.dataService.getDATA(this.dataService.appConstant.API["DELETE_EMPLOYEE"]+id).subscribe(response => {
          this.apiResponse = response;
          this.dataService.appConstant.hideLoader();
          if (this.apiResponse.status === 200) {
            this.empList = this.apiResponse.response;
            this.getEmpList();
          }
        })
      }
    })
  }
  

  /***
   * This method use to prepare employee object for edit
   * 
   */
  prepareEdit(emp){
    this.formDATA= emp;
    $("#myModal").modal("show");
  }


   /***
   * This method use to get only number field in mobile and zip code
   * 
   */
    // number validation pattern
    public validatePattern(event): void {
      if (!/[0-9]/.test(String.fromCharCode(event.charCode))) {
        event.preventDefault();
      }
    }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../service/dataService/dataService';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formDATA:any = {};
  public data:any;
  public openTab:any;

  constructor(
    public router: Router,
    public dataService:DataService
  ) {
    this.openTab = 'login'
  }
  ngOnInit() {}




  login(loginForm: NgForm) {
    this.dataService.appConstant.showLoader();
    this.dataService.postDATA(this.dataService.appConstant.API['SIGN_IN'], this.formDATA).subscribe((response => {
      this.data = response;
      this.dataService.appConstant.hideLoader();
      if (this.data.status === 200) {
        this.dataService.appConstant.successTost('Login Successfully');
        this.formDATA = this.data.response;
        localStorage.setItem('session', JSON.stringify(this.formDATA));
        this.dataService.sendDataMessage('logged');
        this.router.navigate(["/app/home"]);
        loginForm.onReset();
      } else {
        this.dataService.appConstant.errorTost(this.data.message);
      }
    }), err => {
      this.dataService.appConstant.errorTost(this.data.message);
    });
  }

  doSignUp(signUPForm: NgForm) {
    let name = this.formDATA.name.split(' ');
    this.formDATA.firstName = name[0];
    this.formDATA.lastName = name[1];
    this.formDATA.registerBy = 1;
    this.dataService.appConstant.showLoader();
    this.dataService.postDATA(this.dataService.appConstant.API['SIGN_UP'], this.formDATA).subscribe((response => {
      this.data = response;
      this.dataService.appConstant.hideLoader();
      if (this.data.status === 200) {
        this.dataService.appConstant.successTost('Registration Successfully');
         this.openTab = 'login';
        signUPForm.onReset();
      } else {
        this.dataService.appConstant.errorTost(this.data.message);
      }
    }), err => {
      this.dataService.appConstant.errorTost(this.data.message);
    });
  }


}

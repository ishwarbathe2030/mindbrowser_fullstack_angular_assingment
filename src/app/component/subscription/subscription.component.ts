import { Component, OnInit } from '@angular/core';
import { WindowRef } from '../../service/razorpay-service/windowRef.service'
import { DataService } from '../../service/dataService/dataService';
import { ConfirmationDialogService } from 'src/app/component/confirmation-dialog/confirmation-dialog.service';

declare var $: any;

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {


  public tab: any;

  public packages: any = []
  public apiResponse: any;
  public sessionUser: any = {};
  public profileFile: any;
  public selectedIndex: any;
  public isLoading: boolean = false;
  public buy: any = {};
  public paymentResponse: any = {};
  public filter: any = {};

  constructor(
    private dataService: DataService,
    public confirmationDialogService: ConfirmationDialogService,
    private winRef: WindowRef,) {
    this.sessionUser = JSON.parse(localStorage.getItem('session'));
    this.filter.userId = this.sessionUser.userId;
    this.getPackages();
    this.tab = 'newSubscription'
  }

  ngOnInit(): void { }

  /**
   * Method User to change tab
   * 
   */

  changeTab(tab) {
    this.tab = tab;
    if (tab == 'newSubscription') {
      this.getPackages();
    } else {
      this.getList();
    }
  }

  getPackages() {
    this.filter.userId = this.sessionUser.userId;
    this.dataService.appConstant.showLoader();
    this.dataService.postDATA(this.dataService.appConstant.API["SUBSCRIPTION_PACKAGE_ACTIVE_LIST"], this.filter).subscribe(response => {
      this.apiResponse = response;
      this.dataService.appConstant.hideLoader();
      if (this.apiResponse.status === 200) {
        this.packages = this.apiResponse.response;
        this.packages.forEach(element => {
          if (element.longDescription) {
            let item = element.longDescription.split(",")
            element.benefits = item;
          }
        });
      }
    })
  }

  /**
   * Validate Package And save into backend server
   * 
   */

  buyPackage(item) {
    this.buy = item;
    this.buy.userId = this.sessionUser.userId;
    this.dataService.appConstant.showLoader();
    this.dataService.postDATA(this.dataService.appConstant.API["BUY_SUBSCRIPTION"], this.buy,).subscribe(response => {
      this.apiResponse = response;
      this.dataService.appConstant.hideLoader();
      if (this.apiResponse.status === 200) {
        this.paymentResponse = this.apiResponse.response;
        this.proceedToPay();
      }
    })
  }


  /**
   * Method  Use To Make Payment 
   * 
   */

  proceedToPay() {
    let options: any = {
      description: 'Mind Browser Subscription',
      image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABPlBMVEX///8AVKb4lR6oqq3uHSUAUqUAV6oAVKh+pM7l7vZiksYAWKnB0uby9vmEps8ub7UAT6Rll8oATaP4+vwgYaz///34kAD4khEOYK9Fgr7+79/wHymuyOJvnc3/+ve8vb/uFR7vKzLU4O2Wudv5myT+8eza3uOsxuL5tbGlvdv94MD6pj75ni6xsrXZ5fH2iI3949P7sFk7ebv6qEHP0tX96uP8yZq7wsrtAArxP0j5n4342t1WjcT91q792LD949b8yZH5p1n5okv6sm39zJb8z7H8uGj80777rlL7uH7G2OqOstc7fLz8w5n93sb8wHr7yar5qWf8v4v6tXr5xsX3m5r7vqj0WmD2fnX92b381sf94t7yUFb0b3X46uvEztj4oaT0Y2jyNkD2fYP6sJ36wrj2iX75sa74lon3ipD2Vhq2AAAQSElEQVR4nO2dDVvayBbHQRhCCCGRJBAQIi+CCyJF1FqvuLW6Wgtoe63dVqt9cXW7+/2/wJ1JAiTkZQZbIN4nf7eLT5sh+XHOnHNmMhkCAV++fPny5cuXL1++fPny5cuXL1++fPny5cuXL1++fPl6kpLmfQFTk1Rp1ev7z9HP7/X6y8q8r+cXq1I/ePvqcG2tsbCwsrLQWFs7fHX0x9X/C6WgHJ9sQLT0gkHpdBqCbhxsP3mnFZTW27VU2kRn5ExtvM4q877In1H2+FUq5YA3gDx93nqqjFLl6mjBFU9jXNl4fj5NRinAQgm/Poor9TcreD6VsXG6P6WoQze7uXIvEY1GE71avDlZY5alm80m6/DJSNvPTxtEfBrjmymYkc4lYpk2JzMRBv0ncplojSZsy5ZWE7FOBioWLTftIM+PGmQGHGhjH4Mo0AKN3M3wQo9eWNZ8tMSuxjiRoQAAQaAqGAwCSm5Huzg4Sej2YFuZAbzWkBE7q5ZPZn9zIjxVb7OuJ+7JbmJixmPZZp4LMhqWUYg2slhyOw1b6vUjKhgwNAL9nIlROZvQgKrSJ+dup44CQMGLZuAZTS9AfYlkjBcZFZOjKxxXUo6WBIeT0Lk8x/PWpoDnY91Ro+yjAFFvdEOMUsaLBvqfwQs/JIRG4HgnOq0B08mxdqegcx2RtxhebxRprw4aZU8eBwgR11wQEaGLBoTNXlt2PVD9TPo1KyIbXxTdzgHEntYo+/axgBDx1DmkEhFK8ZiI40Oi2rnx9y9F24y76SmxjA78crD2aECIuOFY4LgTMiqhVGtHSAChFdvmeMPW+gyFacSrhJWztcfzQa0cOUVUd0KACOkEkQHV40HGGBy7JKaX89BLlf2fA1xwThp4GzYX3d3MrEh0+NZ0T3SIL0YlM8jsdfI6xklr7+xDuTshjKXNDpmH6gKy3hWFeIfhCY7n0PHbGz/RB3WlG1ePIYw1o5NYECrZUUMjW267xzBdTB66tXLx8yZE0Wb7EYT9HjZJjEkzCl0WI0RH97uwOq2v/ALAhYXU2SMijcgReNpYm0U6QEdJuiBUpAc7j2Ljo+lUqqEp5TjUt6jR0qkUhZQwOKGLqoRcnE0QWh6g7CLtW+gWGqdnZ/V6/fy8Xj8+e3Oadh/wDxseqWSV359XiAkfIybfI00vDEqFL0/Ml5/avDjPVkZmUJRs690mEWOjrlTqB68afxB76aMEZJn0yHYXpUJjL0ynGu8q2sCRpQ25Vbk6aaTwiKcHhytjIWcKhI7DD4v4KOyF2yfGS9x8pzqYwJaWl7fMw8fzt/iqAM02LqT3ySPNdKXmTmXfkCkOD9TCRKCrO6HQ1ngGV+pExXn6ZIJYOl3xHZQL3wwvrbGhjRCqW+vrNoDqJM4mwSTctWcI1TjTOhxc88qFOspjq4hvfct2rKnUsSOQlTeKVwgB1zU66cpzNcJUdyBfaP2rLSA04/mme8BJn7bMLeZIyKP6rvJ+CIg+eqG6HELacZ69a/3X1Yra+3iCEFBoGJLV65kB4LoKuF51BIQx1c1R06fjg6g5EnI1dLlaMmxcVEyATnNaquqHzoSNcRNOREhWapITNtH8odar3quAXzVANx9FUg4cK/WVC8s0PzEh4PmINhX8iwAZNB1Qea963KEaRXULhkJuPqpkry5OHQnX9i0NCAkBEDuxaCIW44Luc4q2jXk+mbQ0Y9DsRfZUJVRdawi47Oijynb9+WnD+cbNygE04VhrIkLAi7FcE03zN+O9/iRmBIBPUmK/k8930HSyUTIaOLVQrkgfIdeil0NYEyqvG67DDJQptsfGwSSEgGrHhz1DaEbJpzUAJWfK8ZL64aApZRNhDRKeI0J1ylqohrAmRMsWzjacTZg+gMdcvp6cMNkxzRGyqzLZsBhEmHycHV6vEO8b2gExDq/4GF5t6q3JR0OuN0BgPzw+WrO/gZrehL6gfPjTfE+LgJBvj4U2iXD8x3M5szm6mREiENHtquMUmiRD+DtDE2Jv1SnXB6d24/4Ust7ex2eT2hCO2cdPweYZEkJudbxhjhueLYKG9yhZqJ+8wYQuTjrS9usTS0DVfOHH0qSEIJK3fqjdNoER5YSltpRio7trajq8gKYwmxDjpENVzv94dWjqkiuoO29/Kjwzf0R4wnFXQ6IX8UZUB/DjWh0aMdJHhCdpLc4MwwyBkw6EUsemgRFlCuny5sXEhBmbC2V7BBMVMZtrbbYHPVGd/1fepNVCUtgaEToMKuwkKefPjxpa+khvojFF5UNxUkLALNpcqFDDxxq5bHOtbH8wjUq1kQ3fpxsXsPeww1wY+krSDUdSKq2LNcSo1mvS9W5hUi8FjE03DATiWEIgWuIMIowN5icjKuFFqvFOMTrppIQq5LuTxsLRS/ir8G8hXJgw0gDGGi8QIfdIwsVBuQA4FFKOU406fPk6ctLJCaGkq7e/o0B6XZwlIWe5W2oiZPR8iOaphZGTulbdOAk3hXC48OesCMW4zXoZgw1RTQOrto2sqRsu/wzhsxfhcLg4O0J3G2p1aWsFEY6KbvfRPUbXxTAinLAunR4hg8YW2U1E2PwVNpQ+f4c+Gi7smicTp0po46XCyEupvDoRhdJhaf0X9MPPz5aQCQs3n2dHaNNOyAyyBWD6MA1JZyrhCHDdcdkRRjDXh1XC2y8zI7SLNEIsYipMA1cbZsLH2lD5jwYYLv5nwtnEqeVDSIimvLMb52bCR+VDCLhU0Akfxj7YWWeLkQ2D6nxp5U1dMhFOUpfaABZu9sb+bW79UL8xI+1fBX6a8MsQMFz4NNYN5xdL9fsWgfNjxZgtYKiZGBBG0QFg+O5+0hnhaRIGe/BvKq8rJkLSEfBQysNtcQhYuLkcP+usvdRAGOTVIWIra6zaQqEdFv4d8YMG0ucPu2GDPlnW1MytLkWHyMhNlZZkrLxDoWZAerj99pqIsfXt+5IRsPjN0mx+2QIZMQ9Tg1CRDKMnFGsg9T+F4vdvOEMqkO9FIWwyoXX53qwzvolQm8oxj4Ch0Gz/30uFu7vbD9dfKnaYklJ5+fDh9m6ML1z8YT3nPPuhfp87EDB3RJQwpL3bQqGwVNy9+ev+cu/zF0VRJFWKUvnyee/h/na3uFQY4wsXPtqswJxjLEUHZbTHUcwdEVVu0t6uOlIoFIvF3Y+3/9zf/6bq/v7v24+7xaKFTjXhbzaXOl9CbQgVGHdTlPWly+8vBqYpLBWWhipYbafrxV92z6jM10uHk6rsjglxB53zeohIpsLuZ5szzjfSoKyvvb1QXTcSqiN9ac+cCbCAD3aA880W2mGqn9JmI2qTGXu3Sw4OaQO4ZKnXPGFDOMLIaGWa2Yg6YuX+jhTQWpBOn5CkH44m1Q0T+wZE5cd3p7hCCDjvWKoeWFP/iV62Q5T2/r7BB5zCzY/HPjMzA8JgRNTi6ZifDpa2VS4/Fd3NWAjf7jkXeHP30iAaY9h2RbSuRo1ClYdPd86MhaXd+/FR74wIySINOpTSFgoIFsRlbXGUtP3n96JtGQNLu91/rl0r9HlnC036fWYr4uCGqSRc/3tzB6uZESb6/cXd7Q93Pm/YEFlxUStQq8sWxGVan31TWg///nVzU9R0d3P717cf5/hHyb3QD9HRsuaoErtjNeMWrV+CIGWz179dwgL88vKylVWIBskeiKX6eTpaRKWtnhpa3qmyhmlUOIgiQfMYITxRW18SUbWaMbS+vlWtPmYm1TNeihrwXFkLK0J1x4KIIHe2qjQtaNYUoOZOOEGkGTTJ67sQsNUdS8jRMZe3dNGEJp1xtnAlRPejBtszCHRp2R5SC7ClQYTFWtJLNkSeynTig8BZoatb1h6p4bE6WTPaw60w8k4/1JtR4uJoEwKBpaE/rq+vr2suCgMOcs+B+ZqJNsXEMJPknomlw3YgwiW6Q8MokJKl6aom40YdbKmWifBoNbX7nhyeI0RNI1w+Z9wTxib/NXOJtvacMUP1LYsnZ0Vo046IEK2dljOJ1RJrG0ag68Z7HTk5XKvKc25W9KIN1eZ8RMzky7kuTY8wJYGmS91yPsMxxsXmgO+77ALkSRtqbwB7JCO2O/louba6utqFf2rRfKfPMZHxtfQAxJwRvZUtxt9D3eaHD8oMJzMMIwd50xY3xpPZXqXHbWgERaSy8944TqvoNELc28+pH2rvAXh++CSKazsQ7Dj5aZTCyIkQ1476SUIQpGSOa7fbokwRPFkM5J6Dn67GcKrZhGyplMe2y9vVGqSE6FGURC7eLZVK8dUYR4DIOyV+WC9gZPvRCNhmxig/MSFMh7Xm8MR0LobfwQDYrrqevcgIKdG8q5ZQWsQj8pkJt8abjsiqNmufInj8AciuxdusREIIxJo1RpWwwTuYzD9yWeMvFQEhoKJ2HSqH3Q0GcMQPo0xRBIQORabQSWKN6AU3JSCM2PgoUhfbMtmbMY2dCAidihN2EbsvWme2MLbCE8plp6Y5XMYAogdCDZaQbztmtVIG8+gqAB4INThCEFl0rExo7DgIYHdRnb6whHLZ0dOwD8wBYDdFO2NhvdT6SO5I2CEb8EAwxRFG3GZcSn0cYdS58ayEtaHdg6cDNTuYfAHysyNxEo6QciNkcXsdgJhz41kJRyjb1qS62ARmgPEECB2qbl1sAvN89RMgdJj30vUkCAN5TD/MuxHidhr1RKTBEbpFGjqPI/RCtsARdtwIY6583sj4OC8FzoU3zIfY0ttpXDJDYetSt6qt23Yn9ERdGsAlbbnnUnlj6lIAHG9ezE7Yqo1yHj2xCVxJ44XRE64fut3PxXbDYMQLI2AcoYub5nC7jQDZA98ChSekMg6GYKM4EwJutjD2whIGg3Y3u6Ca2FnvpAdKGpKdt/i+/RIgrAm9MV+KzRZQYNHOiCX83aekB5IF2Z0ZOWFFFPrYzdOB6IkbiES7GIqWm51sAjfhjW4gPhlCZMWm6Q5pM0Gw/T3vgZFFgHQ3UYrpjO5yC0R3uUffrDFnke6XyoidaC3eLTW78d4iR3JrfPDkzbxFuuctABCS62cyHMcwRDum8m4TIDPUBPsWq2u+grarvOyO1h/VnLumtfc0iLjNDsxSUyMUndZEzVpTI+yUPDCwQJoWoeOytplrSoRUxxupIjAtQiB6YJZN11QIAXCbKp+xpkLIix7JFEhT+eYe2wXX89I0vn3J9hmyuQnzbYTy5F8SBjhvLJ0dyJ1Q7vTJvy1K4wOczZefzlOuhECsEWwybWoRbHsMEGfDGjvR1xECfvRlu14RjjAglMmGgypgEvNk3jyEJQwEVkX8rJMGKHoo0Q/l3g9VwkA3JpPEGznjqSwxkCshJetb1fQysrurAsD0E54ptk2KIg8cXjwwveheCsV2E23Z5St7eKod7XrQQ6GEKMNTSfjDU5T+i/FFHvodW+pl7L/PGwBK5vJxb/JBxaNu6hkcj23mFrnI2LcoAT7Jc4s9j9pPE9rtQf0ZexHU/5mPZZvxXkekkkleVTJJie3OatPLeBNLkgSIWS5H8/loubwat3/22ZcvX758+fLly5cvX758+fLly5cvX758+fLly5cvX758+Xry+h+V4wspDAFREwAAAABJRU5ErkJggg==',
      currency: 'INR',
      key: 'rzp_test_1DP5mmOlF5G5ag',
      amount: this.paymentResponse.amount * 100,
      name: this.paymentResponse.name,
      prefill: {
        email: this.paymentResponse.email,
        contact: this.paymentResponse.contact,
        name: this.paymentResponse.name,
      },
      theme: {
        color: '#0a706d'
      },
      modal: {
        ondismiss: function () {
          console.log('dismissed')
        }
      }
    };
    options.handler = ((response) => {
      options['payment_response_id'] = response.razorpay_payment_id;
      this.paymentResponse.transactionId = response.razorpay_payment_id
      this.update();
    });
    let rzp = new this.winRef.nativeWindow.Razorpay(options);
    rzp.open();
  }



  /**
   * Save Data After Payment Success To backend server
   * 
   */
  update() {
    this.dataService.appConstant.showLoader();
    this.dataService.putDATA(this.dataService.appConstant.API["UPDATE_SUBSCRIPTION"], this.paymentResponse,).subscribe(response => {
      this.apiResponse = response;
      this.isLoading = false;
      if (this.apiResponse.status === 200) {
        this.dataService.appConstant.hideLoader();
        this.paymentResponse = this.apiResponse.response;
        $("#myModal").modal("show");
        this.getPackages();
      }
    })
  }



  /**
   * GET MANAGER ALL SUBSCRIPTION
   * 
   */
  getList() {
    this.dataService.appConstant.showLoader();
    this.dataService.getDATA(this.dataService.appConstant.API["USER_SUBSCRIPTIONS"] + this.sessionUser.userId).subscribe(response => {
      this.apiResponse = response;
      this.dataService.appConstant.hideLoader();
      if (this.apiResponse.status === 200) {
        this.packages = this.apiResponse.response;
        this.packages.forEach(element => {
          if (element.longDescription) {
            let item = element.longDescription.split(",")
            element.benefits = item;
          }
        });
      }
    })
  }


  /**
   * CANCEL SUBSCRIPTION
   * 
   */
  cancelSubscription(subscriptionId) {
    this.confirmationDialogService.yesNoConfirm('Are you sure you want to cancel subscription?..', '')
      .then((confirmed) => {
        if (confirmed == true) {
          this.dataService.appConstant.showLoader();
          this.dataService.getDATA(this.dataService.appConstant.API["SUBSCRIPTION_CANCEL"] + subscriptionId).subscribe(response => {
            this.apiResponse = response;
            this.dataService.appConstant.hideLoader();
            if (this.apiResponse.status === 200) {
              this.dataService.appConstant.successTost("Subscription Cancelled Successfully")
              this.getList();
            }
          })
        }
      })
  }

  /**
   * RESUME SUBSCRIPTION
   * 
   */
  resumeSubscription(subscriptionId) {
    this.confirmationDialogService.yesNoConfirm('Are you sure you want to resume subscription?..', '')
    .then((confirmed) => {
      if (confirmed == true) {
        this.dataService.appConstant.showLoader();
        this.dataService.getDATA(this.dataService.appConstant.API["SUBSCRIPTION_RESUME"] + subscriptionId).subscribe(response => {
          this.apiResponse = response;
          this.dataService.appConstant.hideLoader();
          if (this.apiResponse.status === 200) {
            this.getList();
            this.dataService.appConstant.successTost("Subscription Resumed Successfully")
          }
        })
      }
    })



  }

}

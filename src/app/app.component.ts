import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../app/service/dataService/dataService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mindbrowser';

  public sessionUser: any = {};
  public isShowSidebar:boolean = false;

  constructor(
    public router: Router,
    public dataService:DataService) {
    this.checkSession();
    this.dataService.getDataMessage().subscribe((msg) => {
      if(msg == 'logged'){
            this.sessionUser = JSON.parse(localStorage.getItem('session'));
      }
     });
  }


  /**
   * This method check user is logged or not
   * If Data are present in localStorage then user is logged 
   * If Data not present in localStorage then user not logged and navigate to login page
   */

  checkSession(){
    if (localStorage.getItem('session')) {
      this.sessionUser = JSON.parse(localStorage.getItem('session'));
    } else {
      this.router.navigate(["/app/login"]);
    }
  }


}

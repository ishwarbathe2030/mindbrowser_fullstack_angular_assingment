import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { NgxSpinnerService } from "ngx-spinner";

@Injectable()
export class AppConstant {
  public BASE_URL: string = environment.baseUrl;
  
  

  constructor(
    private spinner: NgxSpinnerService
  ) {}

  
  // pattern
  public emailPattern = '^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  public zipCodePattern = '^[1-9][0-9]{5}$';
  public mobileNoPattern = '^[6-9][0-9]{9}$';


// Code to show loader when api call is happing
  showLoader(){
    this.spinner.show();
 }

  hideLoader(){
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 500);
 }



//Code to show success tost
  successTost(message){
    var x = document.getElementById("success-snackbar");
    x.innerHTML =
      '<i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;' +
      message;
    x.className = "show";
    setTimeout(function () {
      x.className = x.className.replace("show", "");
    }, 3000);
  }

  //Code to show error tost
  errorTost(message){
    var x = document.getElementById("error-snackbar");
    x.innerHTML =
      '<i class="fa fa-window-close" aria-hidden="true"></i>&nbsp;&nbsp;' +
      message;
    x.className = "show";
    setTimeout(function () {
      x.className = x.className.replace("show", "");
    }, 3000);
   }


   /**
    * ALL API  LIST
    */
  public API: any = {
    SIGN_UP:"authentication/sign-up",
    SIGN_IN:"authentication/login-with-password",

    BUY_SUBSCRIPTION:"subscription/add",
    UPDATE_SUBSCRIPTION:"subscription/update",
    USER_SUBSCRIPTIONS:"subscription/user_subscription_list/",
    SUBSCRIPTION_LIST:"subscription/list",
    SUBSCRIPTION_PACKAGE_ACTIVE_LIST:"subscription-package/web/list",
    SUBSCRIPTION_CANCEL: "subscription/cancel-subscription/",
    SUBSCRIPTION_RESUME:"subscription/resume-subscription/",

    ADD_EMPLOYEE:"employee/add",
    UPDATE_EMPLOYEE:"employee/update",
    DELETE_EMPLOYEE:"employee/delete/",
    LIST_OF_EMPLOYEE:"employee/employee-list/",


  };
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { HomeComponent } from './component/home/home.component';
import { SubscriptionComponent } from '../app/component/subscription/subscription.component';

const routes: Routes = [
  {
    path: 'app/login',
    component: LoginComponent,
  },
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'app/home',
    component: HomeComponent,
  }, 
  {
    path: 'app/subscription',
    component: SubscriptionComponent,
  }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

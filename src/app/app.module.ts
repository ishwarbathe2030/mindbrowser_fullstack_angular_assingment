import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { HomeComponent } from './component/home/home.component';
import { HeaderComponent } from './component/header/header.component';
import { DataService } from '../app/service/dataService/dataService';
import { WindowRef } from '../app/service/razorpay-service/windowRef.service';
import { HttpClientModule } from '@angular/common/http';
import { AppConstant } from '../app/service/app-constant/app-constant'
import { NgxSpinnerModule } from 'ngx-spinner';
import { SubscriptionComponent } from './component/subscription/subscription.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

import { ConfirmationDialogComponent } from './component/confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './component/confirmation-dialog/confirmation-dialog.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    SubscriptionComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserAnimationsModule,
    NgxSpinnerModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    DataService,
    AppConstant,
    WindowRef,
    ConfirmationDialogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
